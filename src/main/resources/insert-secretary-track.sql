use Quileia

insert into dbo.secretary(name) values ('Bogota');

insert into dbo.secretary(name) values ('Cali');

insert into dbo.track(level_congestion,number,street_or_carrer,type)
values (70,120,'Carrer','Calle');

insert into dbo.track(level_congestion,number,street_or_carrer,type)
values (70,170,'Calle','Autopista');

insert into dbo.track(level_congestion,number,street_or_carrer,type)
values (70,170,'Calle','Calle Principal');
