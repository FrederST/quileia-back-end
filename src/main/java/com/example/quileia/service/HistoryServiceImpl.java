package com.example.quileia.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.quileia.entity.Agent;
import com.example.quileia.entity.HistoryAssignation;
import com.example.quileia.entity.Track;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.repository.HistoryRepository;


@Service
public class HistoryServiceImpl implements HistoryService {

	@Autowired
	HistoryRepository historyRepository;
	
	@Override
	public Iterable<HistoryAssignation> getAllHistory() {
		return historyRepository.findAll();
	}

	@Override
	public HistoryAssignation createHistory(HistoryAssignation history) {
		return historyRepository.save(history);
	}

	@Override
	public long countAssignationsToTrack(Agent agent, Track track) {
		return historyRepository.countByAgentAndTrack(agent, track);
	}
	
	@Override
	public HistoryAssignation getHistoryByAgentAndTrack(Agent agent, Track track, boolean active) throws NotFoundException {
		return historyRepository.findByAgentAndTrackAndActive(agent, track, active).orElseThrow(()-> new NotFoundException("History does not exist"));
	}
	
	@Override
	public HistoryAssignation getHistoryById(int id) throws NotFoundException {
		return historyRepository.findById(id).orElseThrow(()-> new NotFoundException("History does not exist"));
	}
	
	@Override
	public HistoryAssignation updateHistory(HistoryAssignation history) throws NotFoundException {
		HistoryAssignation dbHistory = getHistoryById(history.getId());
		mapHistory(dbHistory);
		return historyRepository.save(dbHistory);
	}
	
	private void mapHistory(HistoryAssignation dbHistory) {
		dbHistory.setFinalDate(LocalDateTime.now());
		dbHistory.setActive(false);
	}

	@Override
	public void deleteHistoryOfAgent(Agent agent) {
		Iterable<HistoryAssignation> history = historyRepository.findAllByAgent(agent);

		historyRepository.deleteAll(history);
	}

	@Override
	public Iterable<HistoryAssignation> getHistoryByAgent(Agent agent) {
		
		return historyRepository.findAllByAgent(agent);
	}

	@Override
	public Iterable<HistoryAssignation> getHistoryByTrack(Track track) { 
		
		return historyRepository.findAllByTrack(track);
	}


}
