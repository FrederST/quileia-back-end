package com.example.quileia.service;

import com.example.quileia.entity.Track;
import com.example.quileia.exception.NotFoundException;


public interface TrackService {
	
	public Iterable<Track> getAllTracks();
	
	public Track createTrack(Track track);
	
	public Track getTrackById(int id) throws NotFoundException;
	
	public Track updateTrack(Track track) throws NotFoundException;
	
	public void deleteTrack(int id) throws NotFoundException;
	
	public Iterable<Track> getAllTrackslevelCongestionGreaterThan(double levelCongestion);
	
}
