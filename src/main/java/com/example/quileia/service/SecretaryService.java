package com.example.quileia.service;

import com.example.quileia.entity.Secretary;
import com.example.quileia.exception.NotFoundException;

public interface SecretaryService {

	public Iterable<Secretary> getAllSecretarys();
	
	public Secretary getSecretarysById(int secretaryId) throws NotFoundException;
	
	public Secretary createSecretary(Secretary secretary);
	
	public Secretary updateSecretary(Secretary secretary) throws NotFoundException;
	
	public void deleteSecretary(int id) throws NotFoundException;
	
}
