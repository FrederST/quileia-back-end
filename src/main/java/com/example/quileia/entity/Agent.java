package com.example.quileia.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.example.quileia.dto.AgentDTO;


@Entity
public class Agent implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7468460910682438100L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="native")
	@GenericGenerator(name="native",strategy="native")
	private int id;
	
	@Column
    private String name;
	
	@Column
    private String lastName;
	
	@Column
    private double yearsExperience;
	
	@OneToOne()
	private Track currentTrack;
	
	@OneToOne()
	private Secretary secretary;
	
	public Agent() {
		super();
	}
		

	public Agent(int id) {
		super();
		this.id = id;
	}

	

	public Agent(int id, String name, String lastName, double yearsExperience, Track currentTrack,
			Secretary secretary) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.yearsExperience = yearsExperience;
		this.currentTrack = currentTrack;
		this.secretary = secretary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getYearsExperience() {
		return yearsExperience;
	}

	public void setYearsExperience(double yearsExperience) {
		this.yearsExperience = yearsExperience;
	}

	public Track getCurrentTrack() {
		return currentTrack;
	}

	public void setCurrentTrack(Track currentTrack) {
		this.currentTrack = currentTrack;
	}

	public Secretary getSecretary() {
		return secretary;
	}

	public void setSecretary(Secretary secretary) {
		this.secretary = secretary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentTrack == null) ? 0 : currentTrack.hashCode());
		result = prime * result + id;
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((secretary == null) ? 0 : secretary.hashCode());
		long temp;
		temp = Double.doubleToLongBits(yearsExperience);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agent other = (Agent) obj;
		return Objects.equals(id, other.id) &&
				Objects.equals(name, other.name) &&
				Objects.equals(lastName, other.lastName) &&
				Objects.equals(secretary, other.secretary) &&
				Objects.equals(currentTrack, other.currentTrack) &&
				Objects.equals(yearsExperience, other.yearsExperience);
	}

	@Override
	public String toString() {
		return "Agent [id=" + id + ", name=" + name + ", lastName=" + lastName + ", yearsExperience=" + yearsExperience
				+ ", currentTrack=" + currentTrack + ", secretary=" + secretary + "]";
	}

	public AgentDTO toConvertAgentDTO() {
		AgentDTO agentDTO = new AgentDTO();
		agentDTO.setId(this.id);
		agentDTO.setNameA(this.name);
		agentDTO.setLastNameA(this.lastName);
		agentDTO.setYearsExperienceA(this.yearsExperience);
		agentDTO.setSecretaryIdA(this.secretary.getId());
		agentDTO.setTrackIdA(this.currentTrack.getId());
		return agentDTO;
	}
	
}
