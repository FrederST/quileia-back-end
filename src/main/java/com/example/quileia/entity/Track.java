package com.example.quileia.entity;

import javax.persistence.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.example.quileia.dto.TrackDTO;

@Entity
public class Track implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3254613539234247356L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="native")
	@GenericGenerator(name="native",strategy="native")
	private int id;
	
	@Column
	private String type;
	
	@Column
	private String streetOrCarrer;
	
	@Column
	private int number;
	
	@Column
	private double levelCongestion;
	

	public Track() {
		super();
	}

	

	public Track(int id) {
		super();
		this.id = id;
	}


	public Track(int id, String type, String streetOrCarrer, int number, double levelCongestion) {
		super();
		this.id = id;
		this.type = type;
		this.streetOrCarrer = streetOrCarrer;
		this.number = number;
		this.levelCongestion = levelCongestion;
	}


	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getStreetOrCarrer() {
		return streetOrCarrer;
	}



	public void setStreetOrCarrer(String streetOrCarrer) {
		this.streetOrCarrer = streetOrCarrer;
	}



	public int getNumber() {
		return number;
	}



	public void setNumber(int number) {
		this.number = number;
	}



	public double getLevelCongestion() {
		return levelCongestion;
	}



	public void setLevelCongestion(double levelCongestion) {
		this.levelCongestion = levelCongestion;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		long temp;
		temp = Double.doubleToLongBits(levelCongestion);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + number;
		result = prime * result + ((streetOrCarrer == null) ? 0 : streetOrCarrer.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(levelCongestion) != Double.doubleToLongBits(other.levelCongestion))
			return false;
		if (number != other.number)
			return false;
		if (streetOrCarrer == null) {
			if (other.streetOrCarrer != null)
				return false;
		} else if (!streetOrCarrer.equals(other.streetOrCarrer))
					return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
					return false;
		return true;
	}

	

	@Override
	public String toString() {
		return "Track [id=" + id + ", type=" + type + ", streetOrCarrer=" + streetOrCarrer + ", number=" + number
				+ ", levelCongestion=" + levelCongestion + "]";
	}



	public TrackDTO toConvertTrackDTO() {
		TrackDTO trackDTO = new TrackDTO();
		trackDTO.setId(this.id);
		trackDTO.setNumberT(this.number);
		trackDTO.setStreetOrCarrerT(this.streetOrCarrer);
		trackDTO.setTypeT(this.type);
		trackDTO.setLevelCongestionT(this.levelCongestion);
		return trackDTO;
	}
}
