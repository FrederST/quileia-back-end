package com.example.quileia.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.quileia.dto.TrackDTO;
import com.example.quileia.entity.Track;
import com.example.quileia.exception.NotFoundException;
import com.example.quileia.service.TrackService;


@RestController
@RequestMapping("track")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RestTrackController {
	
	@Autowired
	TrackService trackService;
	
	/**
	 * Muestra todos las vías de la base de datos.
	 * @return Retorna un arreglo con todas las vías y retorna codigo HTTP 200
	 * 			En caso de no encontrar vías retorna un arreglo vacío 
	 */
	@GetMapping("/tracks")
	public Iterable<TrackDTO> getAllTracks(){
		List<TrackDTO> result = new ArrayList<>();
		for(Track tr : trackService.getAllTracks()) {
			result.add(tr.toConvertTrackDTO());
		}
		return result;
	}
	
	/**
	 * Muestra las vías que tengan un nivel de congestion (Level_congestion) superior al solicitado (level)
	 * @param level Nivel de congestión limite para traer la vías con un nivel superior a este.
	 * @return Retorna un arreglo con todas las vías con un un level_congestion superior a (level) y retorna codigo HTTP 200
	 * 			En caso de no encontrar vías retorna un arreglo vacío 
	 */
	@GetMapping("/levelCongestionGreaterThan/{level}")
	public Iterable<TrackDTO> getAllTrackslevelCongestionGreaterThan(@PathVariable(name = "level") int level){
		List<TrackDTO> result = new ArrayList<>();
		for(Track tr : trackService.getAllTrackslevelCongestionGreaterThan(level)) {
			result.add(tr.toConvertTrackDTO());
		}
		return result;
	}
	
	/**
	 * Crea una vía en la base de datos.
	 * @param trackDTO Contiene la información dela vía que se va a crear.
	 * @return Una vez se ha creado la vía en base de datos, se retorna con la información de la vía. 
	 *           Se retorna código HTTP 201 en caso de éxito.
	 */
	@PostMapping("/create-track")
	@ResponseStatus(code = HttpStatus.CREATED)
	public TrackDTO createTrack(@RequestBody TrackDTO trackDTO) {			
		return trackService.createTrack(trackDTO.toConvertTrack()).toConvertTrackDTO();	
	}
	
	/**
	 * Muestra información de una vía especifica.
	 * @param id Id de la vía que se desea obtener información.
	 * @return Si la vía se encuentra en la base de datos se retorna su información. Se genera código HTTP 200
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la vía en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@GetMapping("/{id}")
	public TrackDTO getTrack(@PathVariable(name = "id") int id) throws NotFoundException{
		
		return trackService.getTrackById(id).toConvertTrackDTO();
		
	}
	
	/**
	 * Permite actualizar la informavión de una vía
	 * @param trackDTO Contiene la informacción de la vía que se desea editar
	 * @return Una vez se ha editado la vía en la base de datos, se retorna con la información actualizada. 
	 *         Se retorna código HTTP 202 en caso de éxito.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la vía en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@PutMapping("/edit-track")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public TrackDTO editTrack(@RequestBody TrackDTO trackDTO) throws NotFoundException {
		return trackService.updateTrack(trackDTO.toConvertTrack()).toConvertTrackDTO();	
	}
	
	/**
	 * Permite eliminar una vía en la base de datos.
	 * @param id Id de la vía del que se desea eliminar en la base de datos.
	 * @throws NotFoundException Se genera en caso de que no sea posible encontrar la vía en la base de datos.
	 *                           Se genera HTTP 404.
	 */
	@DeleteMapping("/delete-track/{id}")
	public void deleteTrack(@PathVariable("id") int id) throws NotFoundException {
		trackService.deleteTrack(id);	
	}
	
}
