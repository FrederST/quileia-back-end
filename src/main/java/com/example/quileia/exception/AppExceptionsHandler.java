package com.example.quileia.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.quileia.dto.ResponseMessageDTO;



@ControllerAdvice
public class AppExceptionsHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = {NotFoundException.class})
	public ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request){
		ResponseMessageDTO responseObj = new ResponseMessageDTO(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(responseObj, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = {TrackAssignationException.class})
	public ResponseEntity<Object> handleTrackAssignationException(TrackAssignationException ex, WebRequest request){
		ResponseMessageDTO responseObj = new ResponseMessageDTO(LocalDateTime.now(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(responseObj, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
	}
}
