package com.example.quileia.dto;

import com.example.quileia.entity.Secretary;

public class SecretaryDTO {
	
	private int id;
	
	private String nameS;
	

	public SecretaryDTO() {
		super();
	}
	
	public SecretaryDTO(int id, String nameS) {
		super();
		this.id = id;
		this.nameS = nameS;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameS() {
		return nameS;
	}

	public void setNameS(String nameS) {
		this.nameS = nameS;
	}

	public Secretary toConvertSecretary() {
		Secretary secretary = new Secretary();
		secretary.setId(id);
		secretary.setName(nameS);
		return secretary;
	}

}
