package com.example.quileia.dto;

import java.util.Objects;

import com.example.quileia.entity.Agent;
import com.example.quileia.entity.Secretary;
import com.example.quileia.entity.Track;

/**
 * @author User
 *
 */
public class AgentDTO {
	
	private int id;
	
    private String nameA;
	
    private String lastNameA;
	
    private double yearsExperienceA;
	
	private int trackIdA;
	
	private int secretaryIdA;

	public AgentDTO() {
		super();
	}
	
	public AgentDTO(int id, String nameA, String lastNameA, double yearsExperienceA, int trackIdA, int secretaryIdA) {
		super();
		this.id = id;
		this.nameA = nameA;
		this.lastNameA = lastNameA;
		this.yearsExperienceA = yearsExperienceA;
		this.trackIdA = trackIdA;
		this.secretaryIdA = secretaryIdA;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNameA() {
		return nameA;
	}


	public void setNameA(String nameA) {
		this.nameA = nameA;
	}


	public String getLastNameA() {
		return lastNameA;
	}


	public void setLastNameA(String lastNameA) {
		this.lastNameA = lastNameA;
	}


	public double getYearsExperienceA() {
		return yearsExperienceA;
	}


	public void setYearsExperienceA(double yearsExperienceA) {
		this.yearsExperienceA = yearsExperienceA;
	}


	public int getTrackIdA() {
		return trackIdA;
	}


	public void setTrackIdA(int trackIdA) {
		this.trackIdA = trackIdA;
	}


	public int getSecretaryIdA() {
		return secretaryIdA;
	}


	public void setSecretaryIdA(int secretaryIdA) {
		this.secretaryIdA = secretaryIdA;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int resultA = 1;
		resultA = prime * resultA + id;
		resultA = prime * resultA + ((lastNameA == null) ? 0 : lastNameA.hashCode());
		resultA = prime * resultA + ((nameA == null) ? 0 : nameA.hashCode());
		resultA = prime * resultA + secretaryIdA;
		resultA = prime * resultA + trackIdA;
		long temp;
		temp = Double.doubleToLongBits(yearsExperienceA);
		resultA = prime * resultA + (int) (temp ^ (temp >>> 32));
		return resultA;
	}

	@Override
	public boolean equals(Object objA) {
		if (this == objA)
			return true;
		if (objA == null)
			return false;
		if (getClass() != objA.getClass())
			return false;
		AgentDTO other = (AgentDTO) objA;
		return Objects.equals(id, other.id) &&
				Objects.equals(nameA, other.nameA) &&
				Objects.equals(lastNameA, other.lastNameA) &&
				Objects.equals(secretaryIdA, other.secretaryIdA) &&
				Objects.equals(trackIdA, other.trackIdA) &&
				Objects.equals(yearsExperienceA, other.yearsExperienceA);
	}
	

	@Override
	public String toString() {
		return "AgentDTO [id=" + id + ", nameA=" + nameA + ", lastNameA=" + lastNameA + ", yearsExperienceA="
				+ yearsExperienceA + ", trackIdA=" + trackIdA + ", secretaryIdA=" + secretaryIdA + "]";
	}

	public Agent toConvertAgent() {
		Agent agent = new Agent();
		agent.setId(this.id);
		agent.setName(this.nameA);
		agent.setLastName(this.lastNameA);
		agent.setYearsExperience(this.yearsExperienceA);
		agent.setSecretary(new Secretary(this.secretaryIdA));
		agent.setCurrentTrack(new Track(this.trackIdA));
		return agent;
	}

}
