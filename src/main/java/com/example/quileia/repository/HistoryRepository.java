package com.example.quileia.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.quileia.entity.Agent;
import com.example.quileia.entity.HistoryAssignation;
import com.example.quileia.entity.Track;

@Repository
public interface HistoryRepository extends CrudRepository<HistoryAssignation, Integer> {

	public long countByAgentAndTrack(Agent agent, Track track);
	
	public Optional<HistoryAssignation> findByAgentAndTrackAndActive(Agent agent, Track track, boolean active);
	
	public Iterable<HistoryAssignation> findAllByAgent(Agent agent);
	
	public Iterable<HistoryAssignation> findAllByTrack(Track track);
	
}
